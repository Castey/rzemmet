var should = require("should");
var PointsCore = require("./points.js");
var EmmetsManager = require("./emmetsManager.js");

function Core(db) {
    this.db = db;

    this.points = new PointsCore(this);
    this.emmetsManager = new EmmetsManager(this);

    this.linkById = {};
    this.nameById = {};
    this.emailById = {};
}

module.exports = Core;