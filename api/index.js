/*
 *  Author: Nikolay "Alagunto" Tkachenko, 2015
 *  Licensed with Razan source codes license
 */

var should = require("should");

var pointsApi = require("./points.js");
var usersApi = require("./users.js");

var Core = require("../core/index.js");

function Api(db) {
    this.db = db;

    this.points = new pointsApi(this.db);
    this.users = new usersApi(this.db);

    this.routes = {
        '/points/upload': this.points.upload,
        '/points/get': this.points.get,

        '/users/getMatches': this.users.getMatches,
        '/users/areThereNewMatches': this.users.checkMatches
    };

    this.matches = {
    };
}

Api.prototype.init = function() {
    this.core = new Core(this.db);
};

Api.prototype.get_routes = function () {
    return this.routes;
};

Api.prototype.important_error = function (exception, where) {
    console.error('Important error on', where);
    console.error(exception);
    //console.error(exception.stack);
    //console.error('BAD BAD SHIT ON ', where, ': ', exception, "\n", exception.stack);
    //process.exit(1);
};

module.exports = Api;
