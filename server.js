/*
 *  Author: Nikolay "Alagunto" Tkachenko, 2015
 *  Licensed with Razan source codes license
 */
if(module === require.main) {
    console.error("Run the main.js, not me");
    process.exit(1);
}

var should = require("should");

var http = require("http");
var url = require("url");
var AppClass = require("./app.js");

function Server() {
    this.server = null;
    this.app = null;
}

// Starts an http server
Server.prototype.init = function(app) {
    this.should.be.an.instanceOf(Server);

    this.app = app;
    try {
        this.app.should.be.ok;
        this.app.should.be.instanceOf(AppClass);
    } catch (exception) {
        console.error('Server got non-app instance passed as app -.-', this.app);
    }


    var self = this;
    this.server = http.createServer(function(request, response) {
        var req_data = url.parse(request.url, 1);

        console.log("got request for " + req_data.pathname);

        self.app.process_request({
            params: req_data.query,
            pathname: req_data.pathname
        }, function(code, text, headers) {
            console.log(req_data.pathname, ': ', code);

            if(code != 200) {
                response.writeHead(code);
                if(code == 400)
                    response.write("<h1>Bad request, fellow</h1>");
                if(code == 404)
                    response.write("<h1>Not found, mate</h1>");
                if(code == 500)
                    response.write("<h1>Sorry, internal shit</h1>");
                if(code == 501)
                    response.write("<h1>Method is not implemented yet</h1>");
                response.end();
                return;
            }

            response.writeHead(code, {'Content-Type': 'text/html'});
            if(text)
                response.write(text);
            response.end();
        });
    }).listen(4120);
    console.log("Server is successfully listening on port 4120");
};

module.exports = Server;
