/*
 *  Author: Nikolay "Alagunto" Tkachenko, 2015
 *  Licensed with Razan source codes license
 */

// Mocha-steps required

var should = require("should");
var request = require("supertest");
request = request('http://localhost:4120');

describe("/UsersAPI/functionality", function() {
    var testusername = "testuser" + parseInt(Math.random() * 100000);
    var testpassword = "412037555";
    var testbadpassword = "413037555";
    var user_id = 0;

    step("should add test user (name " + testusername + ")", function(done) {
        request
            .get('/users/create')
            .query({nickname: testusername})
            .query({password: testpassword})
            .expect(function(res) {
                should(res).be.ok;

                res.statusCode.should.equal(200);

                res.text.should.be.json;
                var body = JSON.parse(res.text);
                should(body).be.ok;
                body.should.have.properties("code", "id");

                body.code.should.be.equal("ok");

                body.id = parseInt(body.id);
                body.id.should.be.above(0);
                user_id = body.id;
            })
            .end(done);
    });

    step("nickname is already taken so this user should not be creatable anymore", function (done) {
        request
            .get('/users/create')
            .query({nickname: testusername})
            .query({password: testpassword})
            .expect(function (res) {
                should(res).be.ok;

                res.statusCode.should.equal(200);

                res.text.should.be.json;
                var body = JSON.parse(res.text);
                should(body).be.ok;

                body.should.have.properties("code");
                body.code.should.be.equal("already exists");
            })
            .end(done);
    });

    step("now there should be the user findable with this nickname", function (done) {
        request
            .get('/users/get')
            .query({nickname: testusername})
            .expect(function (res) {
                should(res).be.ok;
                res.statusCode.should.equal(200);

                var body = JSON.parse(res.text);
                should(body).be.ok;

                body.should.have.properties("code", "id", "nickname");
                body.id.should.be.equal(user_id);
                body.nickname.should.be.equal(testusername);
            })
            .end(done);
    });

    step("now there should be the user findable with this id", function (done) {
        request
            .get('/users/get')
            .query({id: user_id})
            .expect(function (res) {
                should(res).be.ok;
                res.statusCode.should.equal(200);

                var body = JSON.parse(res.text);
                should(body).be.ok;

                body.should.have.properties("code", "id", "nickname");
                body.id.should.be.equal(user_id);
                body.nickname.should.be.equal(testusername);
            })
            .end(done);
    });

    step("finding user by both nickname and id fields should return error", function (done) {
        request
            .get('/users/get')
            .query({id: user_id, nickname: testusername})
            .expect(400)
            .end(done);
    });

    step("should be able to log in with good password hash", function (done) {
        request
            .get('/users/login')
            .query({nickname: testusername, password: testpassword})
            .expect(function (res) {
                should(res).be.ok;
                res.statusCode.should.equal(200);

                var body = JSON.parse(res.text);
                should(body).be.ok;

                body.should.have.properties("code");
                body.code.should.be.equal("ok");
            })
            .end(done);
    });

    step("should not be able to log in with bad password hash", function (done) {
        request
            .get('/users/login')
            .query({nickname: testusername, password: testbadpassword})
            .expect(function (res) {
                should(res).be.ok;
                res.statusCode.should.equal(200);

                var body = JSON.parse(res.text);
                should(body).be.ok;

                body.should.have.properties("code");
                body.code.should.be.equal("bad credentials");
            })
            .end(done);
    });

    step("should remove the created user", function(done) {
        request
            .get('/users/remove')
            .query({id: user_id})
            .expect(function(res) {
                should(res).be.ok;
                res.statusCode.should.equal(200);

                var body = JSON.parse(res.text);
                should(body).be.ok;
                body.should.have.properties("code");
                body.code.should.be.equal("ok");
            })
            .end(done);
    });

    step("get method should return 'not found' code for this user", function (done) {
        request
            .get('/users/get')
            .query({id: user_id})
            .expect(function (res) {
                should(res).be.ok;
                res.statusCode.should.equal(200);

                var body = JSON.parse(res.text);
                should(body).be.ok;

                body.should.have.property("code");
                body.code.should.be.equal("not found");
            })
            .end(done);
    });
});