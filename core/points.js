var sprintf = require("string-format");

function Points(context) {
    this.context = context;
}

if (typeof(Number.prototype.toRad) === "undefined") {
    Number.prototype.toRad = function() {
        return this * Math.PI / 180;
    }
}

Points.prototype.registerNewPoints = function(data) {
    var self = this;
    var user = data.forUser;
    data.should.have.properties("forUser", "points");
    console.log('Registering new points', data);

    for(var i = 0; i < data.points.length; i++) {
        var point = data.points[i];
        console.log(point);
        (function(point) {
            self.context.db.sql("SELECT * FROM points WHERE CURRENT_TIMESTAMP() - `time` <= 24 * 60 * 60 AND user_id != '" + data.forUser + "'",
                function (rows, fields, errors) {
                    for (var i = 0; i < rows.length; i++) {
                        var lat1 = parseFloat(point["lat"]), lon1 = parseFloat(point["lon"]);
                        var lat2 = parseFloat(rows[i]["latitude"]), lon2 = parseFloat(rows[i]["longtitude"]);
                        var R = 6371000; // metres
                        var φ1 = lat1.toRad();
                        var φ2 = lat2.toRad();
                        var Δφ = (lat2-lat1).toRad();
                        var Δλ = (lon2-lon1).toRad();

                        var a = Math.sin(Δφ/2) * Math.sin(Δφ/2) +
                            Math.cos(φ1) * Math.cos(φ2) *
                            Math.sin(Δλ/2) * Math.sin(Δλ/2);
                        var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));

                        var d = R * c;
                        console.log("Distance with is ", d, "meters");
                        if(d < 200) {
                            console.log("Emmetting users", user, rows[i]["user_id"]);
                            self.context.emmetsManager.usersEmmetted(user, rows[i]["user_id"]);
                        }
                    }
                });
            self.context.db.sql(
                sprintf("INSERT INTO points VALUES (0, '{0}', '{1}', '{2}', FROM_UNIXTIME({3}))",
                    user,
                    point["lat"],
                    point["lon"],
                    parseInt(point["time"])
                )
            );
        })(point);
    }
};

Points.prototype.registerNewPoint = function(data) {
    this.registerNewPoints(data["forUser"], [data["point"]]);
};

module.exports = Points;