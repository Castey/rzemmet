/*
    DLP server is a Data-Level Proxy server which is delegated to serve all requests to database.
    Author: Nikolay "Alagunto" Tkachenko, 2015
    Licensed with Razan source codes license
*/
/*
    This one is designed for usage with mysql
 */

if(module !== require.main) {
    console.error("Should be run as a main module");
    process.exit(1);
}
console.log('Stash Data-Level Proxy server. Current version is 1.0');
console.log('Author: Nikolay "Alagunto" Tkachenko');
console.log('Licensed with Razan source codes license');

var should = require('should');

var ServerClass = require('./server.js');
var ApiClass = require('./api/index.js');
var DbClass = require('./db.js');
var AppClass = require('./app.js');

try {
    // Test if all libraries work
    ServerClass.should.have.type('function');
    ApiClass.should.have.type('function');
    DbClass.should.have.type('function');
    AppClass.should.have.type('function');
} catch (exception) {
    console.error('Cannot resolve main modules =(');
    console.error(exception, exception.stack.split("\n"));
    process.exit(1);
}

console.log('Modules resolved, starting application');

var server = new ServerClass();
var db = new DbClass();
var api = new ApiClass(db);

var app = new AppClass(server, api, db);
app.run();


