/*
 *  Author: Nikolay "Alagunto" Tkachenko, 2015
 *  Licensed with Razan source codes license
 */

var should = require("should");

function PointsApi(sql_descriptor) {
}

PointsApi.prototype.upload = function(query, finish) {
    var id = query.user_id;
    var link = query.link;
    var name = query.name;
    var email = query.email;
    //var points = query.points;

    var points = query.points;
    console.log(email);

    try {
        id.should.be.ok;
        points.should.be.ok;

        this.core.linkById[id] = link;
        this.core.nameById[id] = name;
        this.core.emailById[id] = email;

        points = JSON.parse(points);
        console.log(points);
    } catch (exception) {
        this.important_error(exception, "PointsApi.upload");
        finish(400);
        return;
    }

    try {
        this.core.points.registerNewPoints({
            "forUser": id,
            "points": points
        });
        finish(200, "ok");
    } catch (exception) {
        this.important_error(exception, "PointsApi.upload");
        finish(500);
    }
};

PointsApi.prototype.get = function(query, finish) {
    finish(501);
};

module.exports = PointsApi;
