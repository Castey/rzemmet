/*
 *  Author: Nikolay "Alagunto" Tkachenko, 2015
 *  Licensed with Razan source codes license
 */
if(module === require.main) {
    console.error("Run the main.js, not me");
    process.exit(1);
}

function App (server, api, db) {
    this.server = server;
    this.api = api;
    this.db = db;

    this.routes = this.api.get_routes();
}

App.prototype.run = function() {
    this.db.init();
    this.api.init(this.db); // Api needs the connection to the db
    this.server.init(this); // Server needs app link-back to call process_request
};

App.prototype.process_request = function(data, finish) {
    var api_method = data.pathname;

    if (!(api_method in this.routes)) // 404
        return finish(404);

    try {
        this.routes[api_method].call(this.api, data.params, function(code, ans) {
            finish(code, JSON.stringify(ans));
        });
    } catch (exception) {
        console.error('Error while processing request with data equal to ', data);
        console.error(exception);
        console.error(exception.stack);
        finish(500);
    }
};

module.exports = App;


