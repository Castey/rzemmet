var sprintf = require("string-format");
var should = require("should");

function EmmetsManager(context) {
    console.log("Manager initted");
    this.context = context;
    this.locked = false;
    this.loadFromServer();
    setInterval(
        (function(self) {
            return function() {
                self.saveToServer();
            }
        })(this),
        10 * 1000
    );
}

EmmetsManager.prototype.getEmmetsForUser = function(user, callback) {
    var self = this;
    var timer = setInterval(function(){
        if (!self.locked) {
            complete();
            clearInterval(timer);
        }
    }, 100);
    function complete() {
        try {
            var data = self.emmets[user];
            callback(data);
        } catch (exception) {
            callback(null);
        }
    }
};

EmmetsManager.prototype.areThereNewMatches = function(user_id) {
    return (user_id in this.hasNewMatches && this.hasNewMatches[user_id]);
};

EmmetsManager.prototype.confirmMatchesWereViewed = function(user_id) {
    this.hasNewMatches[user_id] = false;
};


EmmetsManager.prototype.usersEmmetted = function(user1, user2, value, callback) {
    var self = this;
    if (this.context.emailById[user1] == this.context.emailById[user2])
        return;
    if (!value)
        value = 1;
    var timer = setInterval(function(){
        if (!self.locked) {
            complete();
            clearInterval(timer);
        }
    }, 100);
    function complete() {
        try {
            if (!(user1 in self.emmets)) {
                self.emmets[user1] = {};
            }
            if (!(user2 in self.emmets)) {
                self.emmets[user2] = {};
            }

            if(user2 in self.emmets[user1]) {
                self.emmets[user1][user2] += value;
            } else {
                self.emmets[user1][user2] = value;
                self.hasNewMatches[user1] = true;
                console.log("New match for user", user1, ": ", user2);
            }

            if(user1 in self.emmets[user2]) {
                self.emmets[user2][user1] += value;
            } else {
                self.emmets[user2][user1] = value;
                self.hasNewMatches[user2] = true;
                console.log("New match for user", user2, ": ", user1);
            }

        } catch (exception) {
            callback(null);
        }
    }
};

EmmetsManager.prototype.loadFromServer = function() {
    var self = this;
    if(this.locked)
        return;
    this.locked = true;

    this.hasNewMatches = {};

    this.context.db.sql(
        "SELECT * FROM emmets_map_dumps ORDER BY time DESC LIMIT 1",
        function(rows, fields, user_info, err) {
            //should(err).should.not.be.ok;
            if (rows.length > 0) {
                try {
                    self.emmets = JSON.parse(rows[0]["content"]);
                    console.log("Loaded some emmets from server:", self.emmets);
                } catch (exception) {
                }
            } else {
                self.emmets = {};
            }
            self.locked = false;
        }
    );
};

EmmetsManager.prototype.saveToServer = function() {
    if(this.locked) {
        return;
    }
    var data = JSON.stringify(this.emmets);
    this.context.db.sql(
        sprintf("INSERT INTO emmets_map_dumps VALUES (0, '{0}', DEFAULT)", data)
    );
};


module.exports = EmmetsManager;