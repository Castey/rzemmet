/*
 *  Author: Nikolay "Alagunto" Tkachenko, 2015
 *  Licensed with Razan source codes license
 */

if(module === require.main) {
    console.error("FATAL: Run the main.js, not me");
    process.exit(1);
}

var should = require('should');
var mysql = require('mysql');

var params = {
    host: '127.0.0.1',
    user: 'emmet',
    password: 'th3sun1sg01ngd0wn',
    database: 'emmet'
};

function Db() {
    this.mysql_descriptor = null;
}

Db.prototype.init = function() {
    this.should.be.an.instanceOf(Db);
    this.connect();
    this.check_db();
};

Db.prototype.connect = function() {
    this.should.be.an.instanceOf(Db);

    // Testing params to be valid
    try {
        should(params).have.properties('host', 'user', 'password', 'database');
    } catch (exception) {
        console.error('FATAL: Not enough params to connect to database in db.js: ', exception);
        process.exit(1);
    }

    try {
        this.mysql_descriptor = mysql.createConnection(params);
    } catch (exception) {
        console.error('FATAL: Could not connect to database in db.js: ', exception);
        process.exit(1);
    }
};

Db.prototype.sql = function(query, callback, user_data) {
    try {
        this.should.be.an.instanceOf(Db);
        this.mysql_descriptor.should.be.ok;
    } catch (exception) {
        console.error('NON-FATAL: Cannot run sql query: ', exception);
    }

    this.mysql_descriptor.query(query, function(err, rows, fields) {
        try {
            should.not.exist(err);
            callback && callback(rows, fields, user_data, err);
        } catch (exception) {
            console.error('NON FATAL: could not execute query "' + query + '". Error found: ', err);
            callback && callback(null, null, null, exception);
        }
    });
};


// This methomd checks the database for required tables and
// TODO: implement this shit
Db.prototype.check_db = function() {
    this.should.be.an.instanceOf(Db);
    console.log('Checking database...');

    var database_ok = true;
    try {
        this.mysql_descriptor.should.be.ok;
        this.mysql_descriptor.query("SELECT 1", function(){
            console.log("Database responds");
        });
    } catch (exception) {
        console.error('FATAL: cannot check database due to no connection =[');
        process.exit(1);
    }

    if (!database_ok) {
        console.error('NON-FATAL: Database was not set up properly. Trying to fix...');
        this.fix_db();
    } else {
        console.log('Database ok');
    }
};

Db.prototype.fix_db = function() {
    this.should.be.an.instanceOf(Db);
};

module.exports = Db;




