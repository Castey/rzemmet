/*
 *  Author: Nikolay "Alagunto" Tkachenko, 2015
 *  Licensed with Razan source codes license
 */

var should = require("should");
var format = require("string-format");
format.extend(String.prototype);

function UsersApi(sql_descriptor) {
}

UsersApi.prototype.getMatches = function(query, finish) {
   var user_id = query.user_id;
   var self = this;
   this.core.emmetsManager.getEmmetsForUser(user_id, function(data) {
      if (!data)
         data = {};
      var result = {};
      for(var match in data) {
         if(!data.hasOwnProperty(match))
            continue;
         result[match] = {
            "value": data[match],
            "link": self.core.linkById[data[match]] + "",
            "name": self.core.nameById[data[match]] + ""
         };
      }
      finish(200, result);
   });
};

UsersApi.prototype.checkMatches = function(query, finish) {
   var user_id = query.user_id;
   var ans = this.core.emmetsManager.areThereNewMatches(user_id);
   this.core.emmetsManager.confirmMatchesWereViewed(user_id);
   finish(200, ans);
};

module.exports = UsersApi;
